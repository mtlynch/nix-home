# nix-home

Michael's Nix Home Manager config.

## Install

To install on a system without Nix Home Manager:

```bash
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager && \
  nix-channel --update && \
  nix-shell '<home-manager>' -A install && \
  pushd ~/.config/home-manager && \
  rm home.nix && \
  git clone git@gitlab.com:mtlynch/nix-home.git . && \
  home-manager switch -b backup && \
  popd
