{ config, pkgs, lib, ... }:

{
  home.username = "mike";
  home.homeDirectory = "/home/mike";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.

  xdg.configFile."direnv/direnv.toml".text = ''
  # Managed by home-manager
  hide_env_diff = true
'';

  programs.bash = {
    enable = true;
    enableCompletion = true;
    shellOptions = [ "histappend" "checkwinsize" ];
    historyControl = [ "ignoredups" "ignorespace" ];
    historyFileSize = 10000000;
    historySize = 10000000;
    shellAliases = {
      a = ". ./venv/bin/activate";
      au = "sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y";
      calc = "gnome-calculator";
      fs = "sudo df -h /";
      gau = "git add --update";
      ga = "git add --all";
      gaa = "git add --all";
      gags = "ga && gs";
      gs = "git status";
      gd = "git diff --cached";
      gb = "git branch -v --sort=committerdate | tail -n 10";
      gbb = "git checkout $(git branch --sort=-committerdate | grep -v \"^\\* \" | sed -n \"1 p\")";
      gbbb = "git checkout $(git branch --sort=-committerdate | grep -v \"^\\* \" | sed -n \"2 p\")";
      gc = "git commit --message";
      gcbm = "git_sync_and_branch";
      gcp = "git_commit_and_push";
      gco = "git checkout";
      gf = "git fetch";
      gl = "git log";
      gmm = "git merge master";
      gpom = "git pull origin master";
      gss = "git stash save";
      gsp = "git stash pop";
      gu = "git reset --soft HEAD~1";
      gr = "git reset";
      grc = "git rebase --continue";
      grm = "git rebase master";
      gp = "git push";
      gwip = "git commit --all --no-verify --message \"work in progress\"";
      gwipp = "gwip && gp";
      hs = "pushd ~/.config/home-manager && gcbm && home-manager switch && popd";
      ic = "magick";
      n = "nautilus";
      nd = "nix develop";
      ndp = "nix develop path:.";
      rm = "rm --interactive";
      td = "pushd $(mktemp -d)";
      tt = "trash";
    };
    initExtra = ''
      # Enable color console.
      alias ls='ls --color=auto'

      # https://jeffkreeftmeijer.com/nix-home-manager-git-prompt/
      . ${pkgs.git}/share/git/contrib/completion/git-prompt.sh

      # Show git branch status in terminal shell.
      export PS1='\[\033[01;34m\]\w\[\033[00m\]\[\033[01;32m\]$(__git_ps1 " (%s)")\[\033[00m\]\$ '

      # Not sure why this doesn't happen automatically.
      . ${pkgs.git}/share/git/contrib/completion/git-completion.bash

      find_main_branch() {
          local main_branch

          # Get the main branch name by checking for common names.
          main_branch=$(
            git symbolic-ref refs/remotes/origin/HEAD 2>/dev/null \
            | sed 's@^refs/remotes/origin/@@'
          )

          if [ -z "$main_branch" ]; then
              # If symbolic-ref fails, try to detect main/master directly
              if git show-ref --quiet --verify refs/remotes/origin/main; then
                  main_branch="main"
              elif git show-ref --quiet --verify refs/remotes/origin/master; then
                  main_branch="master"
              else
                  echo "Error: Could not determine main branch name" >&2
                  return 1
              fi
          fi

          echo "$main_branch"
      }

      function git_sync_and_branch {
        local readonly TARGET_BRANCH="$1"
        local MAIN_BRANCH="$(find_main_branch)"
        readonly MAIN_BRANCH

        git checkout "''${MAIN_BRANCH}" && \
          git pull origin "''${MAIN_BRANCH}" && \
          if [[ ! -z "$TARGET_BRANCH" ]]; then
            git checkout -b "''${TARGET_BRANCH}"
          fi
      }

      function git_commit_and_push {
        local readonly COMMIT_MESSAGE="$1"

        git commit --message "''${COMMIT_MESSAGE}" && \
          git push
      }

      # Set fly.io CLI environment variables.
      export FLYCTL_INSTALL="/home/mike/.fly"
      export PATH="$FLYCTL_INSTALL/bin:$PATH"
    '';
  };

  programs.direnv = {
    enable = true;
    enableBashIntegration = true;
    nix-direnv.enable = true;
  };

  programs.git = {
    enable = true;
    userName = "Michael Lynch";
    userEmail = "git@mtlynch.io";
    extraConfig = {
      init.defaultBranch = "master";
      push.default = "current";
      pull.rebase = false;
    };
  };

  programs.go = {
    enable = true;
    goPath = "code/go";
  };

  programs.readline = {
    enable = true;
    bindings = {
      # enable partial history search
      "\\e[A" = "history-search-backward";
      "\\e[B" = "history-search-forward";
    };
  };

  programs.ssh = {
    enable = true;
    addKeysToAgent = "yes";
    matchBlocks = {
      "truenas" = {
        user = "root";
      };
      "rm2.home.arpa" = lib.hm.dag.entryBefore ["*.home.arpa"] {
        user = "root";
        forwardAgent = false;
      };
      "*.home.arpa" = lib.hm.dag.entryBefore ["*.*"] {
        forwardAgent = true;
        extraOptions = {
          StrictHostKeyChecking = "no";
        };
      };
      # For hosts on the local network like `foo`
      "* !*.*" = {
        forwardAgent = true;
        extraOptions = {
          StrictHostKeyChecking = "no";
        };
      };
      "*.*" = {
        forwardAgent = false;
        extraOptions = {
          HostbasedAuthentication = "no";
          KexAlgorithms = "curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256";
          Ciphers = "chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr";
        };
      };
    };
    extraConfig = ''
      HashKnownHosts yes
      CheckHostIP no
      IdentityFile ~/.ssh/id_ed25519
      IdentityFile ~/.ssh/id_rsa
      IdentityFile ~/.ssh/id_rsa_github
    '';
  };

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (pkgs.lib.getName pkg) [
    "vscode"
    "vscode-extension-ms-vscode-remote-remote-ssh"
    "vscode-extension-ms-vscode-remote-remote-ssh-edit"
  ];

  programs.vscode = {
    enable = true;
    enableUpdateCheck = false;
    extensions = with pkgs.vscode-extensions; [
      bbenoist.nix
      ms-vscode-remote.remote-ssh
      ms-vscode-remote.remote-ssh-edit
      # Cline
      saoudrizwan.claude-dev
    ];
    userSettings = {
      # Better Defaults (from Make VS Code Awesome)
      "editor.copyWithSyntaxHighlighting" = false;
      "diffEditor.ignoreTrimWhitespace" = false;
      "editor.emptySelectionClipboard" = false;
      "workbench.editor.enablePreview" = false;
      "window.newWindowDimensions" = "offset";
      "editor.multiCursorModifier" = "ctrlCmd";
      "files.trimTrailingWhitespace" = true;
      "diffEditor.renderSideBySide" = false;
      "editor.snippetSuggestions" = "top";
      "editor.detectIndentation" = false;
      "files.insertFinalNewline" = true;
      "files.trimFinalNewlines" = true;
      "workbench.sideBar.location" = "right";
      "workbench.statusBar.visible" = false;
      "workbench.editor.showTabs" = "single";
      "editor.minimap.enabled" = false;
      "editor.lineNumbers" = "on";
      "scm.diffDecorations" = "none";
      "editor.hover.enabled" = false;
      "editor.matchBrackets" = "never";
      "workbench.tips.enabled" = false;
      "editor.colorDecorators" = false;
      "git.decorations.enabled" = false;
      "workbench.startupEditor" = "none";
      "editor.lightbulb.enabled" = "off";
      "editor.selectionHighlight" = false;
      "editor.overviewRulerBorder" = false;
      "editor.renderLineHighlight" = "none";
      "editor.occurrencesHighlight" = "off";
      "problems.decorations.enabled" = false;
      "editor.renderControlCharacters" = false;
      "editor.hideCursorInOverviewRuler" = true;
      "workbench.editor.enablePreviewFromQuickOpen" = false;
      "comments.visible" = false;
      "window.title" = ''''${rootName}''${separator}''${activeEditorShort}'';
      "explorer.confirmDelete" = false;
      "files.eol" = "\n";
      "editor.tabSize" = 2;
      "editor.rulers" = [80];
      "editor.fontFamily" = "JetBrains Mono";
      "editor.fontSize" = 13;
      "editor.fontLigatures" = false;
      "extensions.ignoreRecommendations" = true;
      "[go]" = {
        "editor.snippetSuggestions" = "none";
        "editor.formatOnSave" = true;
        "editor.codeActionsOnSave" = {
          "source.organizeImports" = "explicit";
        };
      };
      "gopls" = {
        "usePlaceholders" = true;
        "ui.semanticTokens" = true;
        "completeUnimported" = true;
        "deepCompletion" = true;
      };
      "[Dockerfile]" = {
        "editor.formatOnSave" = false;
      };
      "[python]" = {
        "editor.tabSize" = 4;
      };
      "[liquid]" = {
        "editor.formatOnSave" = true;
      };
      "[markdown]" = {
          "editor.quickSuggestions" = {
              "comments" = false;
              "strings" = false;
              "other" = false;
          };
          "editor.tabCompletion" = "off";
      };
      "[tsv]" = {
        "editor.insertSpaces" = false;
        "editor.tabSize" = 4;
      };
      "explorer.confirmDragAndDrop" = false;
      "remote.SSH.useExecServer" = false;
      "files.exclude" = {
        "**/*.pyc" = true;
      };
      "files.associations" = {
        "*.mustache" = "html";
      };
      "remote.SSH.lockfilesInTmp" = true;
      "remote.SSH.suppressWindowsSshWarning" = true;
      "workbench.editorAssociations" = {
        "*.ipynb" = "jupyter-notebook";
        "*.y4f" = "default";
      };
      "go.toolsManagement.autoUpdate" = true;
      "git.ignoreMissingGitWarning" = true;
      "terminal.integrated.tabs.enabled" = false;
      # For some reason, VS Code defaults to nano if I don't set this.
      "terminal.integrated.env.linux" = {
          "EDITOR" = "vim";
      };
      "notebook.cellToolbarLocation" = {
        "default" = "right";
        "jupyter-notebook" = "left";
      };
      "telemetry.telemetryLevel" = "off";
      "terminal.integrated.enableMultiLinePasteWarning" = "never";
      "editor.guides.indentation" = false;
      "git.mergeEditor" = false;
      "editor.codeLens" = true;
      "editor.inlineSuggest.enabled" = true;
      "editor.accessibilitySupport" = "off";
      "security.allowedUNCHosts" = ["truenas"];
      "workbench.activityBar.location" = "hidden";
      "explorer.confirmPasteNative" = false;
      "editor.inlineSuggest.suppressSuggestions" = true;
      "cody.autocomplete.enabled" = false;
      "cody.telemetry.level" = "off";
      "openctx.providers" = {};
      "editor.renderWhitespace" = "all";
      "workbench.colorTheme" = "Monokai";
      "cline.mcp.mode" = "off";
    };
    keybindings = [
      # Panels
      {
        key = "ctrl+k ctrl+e";
        command = "workbench.view.explorer";
      }
      {
        key = "ctrl+k ctrl+g";
        command = "workbench.view.scm";
      }
      {
        key = "ctrl+k ctrl+d";
        command = "workbench.view.debug";
      }
      {
        key = "ctrl+k ctrl+x";
        command = "workbench.extensions.action.showInstalledExtensions";
      }
      {
        key = "ctrl+k ctrl+b";
        command = "workbench.action.toggleSidebarVisibility";
      }
      {
        key = "ctrl+e";
        command = "workbench.action.focusActiveEditorGroup";
      }
      {
        key = "ctrl+t";
        command = "workbench.action.terminal.toggleTerminal";
      }
      # Emmet
      {
        key = "ctrl+m ctrl+i";
        command = "editor.emmet.action.balanceIn";
        when = "editorTextFocus";
      }
      {
        key = "ctrl+m ctrl+o";
        command = "editor.emmet.action.balanceOut";
        when = "editorTextFocus";
      }
      {
        key = "ctrl+m ctrl+w";
        command = "editor.emmet.action.wrapWithAbbreviation";
        when = "editorTextFocus";
      }
      {
        key = "ctrl+m ctrl+m";
        command = "editor.emmet.action.matchTag";
        when = "editorTextFocus";
      }
      {
        key = "ctrl+m ctrl+e";
        command = "editor.action.smartSelect.expand";
        when = "editorTextFocus";
      }
      {
        key = "ctrl+m ctrl+r";
        command = "editor.emmet.action.updateTag";
        when = "editorTextFocus";
      }
      {
        key = "ctrl+m ctrl+backspace";
        command = "editor.emmet.action.removeTag";
        when = "editorTextFocus";
      }
      # Terminal toggle
      {
        key = "ctrl+`";
        command = "workbench.action.terminal.focus";
      }
      {
        key = "ctrl+`";
        command = "workbench.action.focusActiveEditorGroup";
        when = "terminalFocus";
      }
      {
        key = "ctrl+down";
        command = "workbench.action.terminal.focusNext";
        when = "terminalFocus";
      }
      {
        key = "ctrl+up";
        command = "workbench.action.terminal.focusPrevious";
        when = "terminalFocus";
      }
      # Explorer new file/folder
      {
        key = "ctrl+n";
        command = "explorer.newFile";
        when = "explorerViewletFocus";
      }
      {
        key = "ctrl+shift+n";
        command = "explorer.newFolder";
        when = "explorerViewletFocus";
      }
    ];
  };

  home.sessionVariables = {
    EDITOR = "vim";
    LANG = "en_US.UTF-8";
    LC_CTYPE = "en_US.UTF-8";
    LC_ALL = "en_US.UTF-8";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
